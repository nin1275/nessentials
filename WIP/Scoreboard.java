package WIP;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Criterias;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;
import org.jetbrains.annotations.NotNull;

public final class Scoreboard implements Runnable {

    private final static Scoreboard instance = new Scoreboard();

    private Scoreboard() {
    }

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getScoreboard() != null && player.getScoreboard().getObjective(ChatColor.YELLOW + "Scoreboard") != null)
                updateScoreboard(player);
            else
                createNewScoreboard(player);
        }
    }

    private void createNewScoreboard(Player player) {
        org.bukkit.scoreboard.@NotNull Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective(ChatColor.RED + "Nessentials", "dummy");

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Scoreboard");

        objective.getScore(ChatColor.WHITE + " ").setScore(7);
        objective.getScore(ChatColor.WHITE + "nin1275 made this").setScore(6);
        objective.getScore(ChatColor.WHITE + "whats better").setScore(5);
        objective.getScore(ChatColor.WHITE + "this or hp under health?").setScore(4);
        objective.getScore(ChatColor.RED + " ").setScore(3);
        objective.getScore(ChatColor.WHITE + "tell me").setScore(2);
        objective.getScore(ChatColor.GREEN + " ").setScore(1);
        //objective.getScore(ChatColor.WHITE + "Walked: 0cm").setScore(0);

        Team team1 = scoreboard.registerNewTeam("team1");
        String teamKey = ChatColor.WHITE.toString();

        team1.addEntry(teamKey);
        team1.setPrefix("Hype: ");
        team1.setSuffix("0");

        objective.getScore(teamKey).setScore(0);

        Objective objectiveHealth = scoreboard.registerNewObjective(
                "_health", Criterias.HEALTH);

        objectiveHealth.setDisplayName(ChatColor.RED + "❤");
        objectiveHealth.setDisplaySlot(DisplaySlot.BELOW_NAME);

        player.setScoreboard(scoreboard);
    }

    private void updateScoreboard(Player player) {
        org.bukkit.scoreboard.@NotNull Scoreboard scoreboard = player.getScoreboard();
        Team team1 = scoreboard.getTeam("team1");

        team1.setSuffix(ChatColor.AQUA + "" + (player.getStatistic(Statistic.WALK_ONE_CM) + player.getStatistic(Statistic.SPRINT_ONE_CM)));
    }

    public static Scoreboard getInstance() {
        return instance;
    }
}