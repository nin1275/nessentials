package WIP;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class HologramCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Only players can use this command!");

            return true;
        }
        Player p = (Player) sender;

        if (args.length > 1) {
            String id = args[0];
            String argsToOneString = String.join(" ", Arrays.asList(args).subList(0, 0).toArray(new String[]{}));
            String filteredtext = Arrays.toString(argsToOneString.split("\\|"));
            //String text = Arrays.toString(String.join(" ", utext).split("\\|"));
            p.sendMessage(ChatColor.RED + filteredtext);
            p.sendMessage(ChatColor.GREEN + id);
        } else {
            return false;
        }
        /*
        Hologram hologram = new Hologram(String.join(" ", args).split("\\|"));


        hologram.spawn(p.getLocation());

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return null;
    }*/
        return true;
    }
}
