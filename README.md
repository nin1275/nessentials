# Nessentials
A small But Essential plugin that is useful for mc servers.
<br><br>
<img alt="License" src="https://img.shields.io/gitlab/license/nin1275/nessentials">
<img alt="Commit activity (main)" src="https://img.shields.io/gitlab/last-commit/nin1275/nessentials">
### development status
<img alt="Commit activity (development)" src="https://img.shields.io/gitlab/last-commit/nin1275/nessentials?ref=development">

## DOWNLOAD AT SPIGOTMC, MODRINTH, OR BUKKIT! SOURCE CODE ONLY UPDATED HERE!

[<img width=500 src="https://cdn.nin1275.xyz/cdn/spigotmcBanner.png">](https://www.spigotmc.org/resources/nessentials.112117)
<br>
[<img src="https://cdn.nin1275.xyz/cdn/modrinth.png">](https://modrinth.com/plugin/nessentials)
<br>
[<img src="https://cdn.nin1275.xyz/cdn/bukkit.png">](https://www.curseforge.com/minecraft/bukkit-plugins/nessentials)

## Goals:

100 - ✅

200 - ❌

500 - ❌

1000 - ❌

## Commands:
/fly

/vanish | /v

/maintenance | /maintenance on | /maintenance off | /maintenance help

/adminchat | /ac

/staffchat | /sc

/adminbroadcast | /abroadcast

/broadcast

/mainmenu | /mm | /menu

/head

/setmotd | /motdset

/godmode

/heal

/nessentials | /ness | /ninessentials

/gms

/gmsp

/gmc

/gma

/suicide

## bStats
<img src="https://bstats.org/signatures/bukkit/nessentials.svg" alt="bstats graph">

Stats:
> Spigot<br>
<img alt="Spiget Version" src="https://img.shields.io/spiget/version/112117">
<img alt="Spiget Downloads" src="https://img.shields.io/spiget/downloads/112117">

> Modrinth<br>
<img alt="Modrinth Version" src="https://img.shields.io/modrinth/v/Dt2MOjqJ">
<img alt="Modrinth Downloads" src="https://img.shields.io/modrinth/dt/Dt2MOjqJ">

> CurseForge/Bukkit<br>
<img alt="CurseForge Version" src="https://img.shields.io/curseforge/v/946433">
<img alt="CurseForge Downloads" src="https://img.shields.io/curseforge/dt/946433">

## IMAGES SOON!!!
