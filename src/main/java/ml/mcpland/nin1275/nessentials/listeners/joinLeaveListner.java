package ml.mcpland.nin1275.nessentials.listeners;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.stuff.UpdateChecker;
import ml.mcpland.nin1275.nessentials.stuff.timerUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.IOException;

import static ml.mcpland.nin1275.nessentials.commands.vanishCommand.vanishedPlayers;

public class joinLeaveListner implements Listener {

    private final Nessentials plugin;

    public joinLeaveListner(Nessentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) throws IOException {
        Player p = e.getPlayer();
        String join = this.plugin.getConfig().getString("Join-message");
        String msg = this.plugin.getConfig().getString("First-Join-Message");
        String motd = this.plugin.getConfig().getString("motd");

        if (join != "") {
            if (vanishedPlayers.contains(p.getUniqueId())) {
                p.sendMessage(ChatColor.GRAY + "You are still in Vanish!");
                e.setJoinMessage(null);
            }else {
                if (p.hasPermission("nessentials.staff") || p.hasPermission("nessentials.admin") || p.hasPermission("nessentials.*")) {
                    String ServerName = this.plugin.getConfig().getString("Server_Name");
                    String msg2 = this.plugin.getConfig().getString("Staff_Join_Message");
                    msg2 = msg2.replace("[player]", e.getPlayer().getDisplayName());
                    msg2 = msg2.replace("[server_name]", ServerName);
                    e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', msg2));
                } else {
                    join = join.replace("[player]", e.getPlayer().getDisplayName());
                    join = join.replace("[server_name]", plugin.getConfig().getString("Server_Name"));
                    e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', join));
                }
            }
        }else {
            p.sendMessage("");
        }

        if (msg != null) {
            if (!p.hasPlayedBefore()) {
                msg = msg.replace("[player]", e.getPlayer().getDisplayName());
                e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
            }
        }

        if (motd != null) {
            motd = motd.replace("[player]", e.getPlayer().getDisplayName());
            e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', motd));
        }

        if (p.hasPermission("nessentials.*")) {
            new UpdateChecker(plugin, 112117).getVersion(version -> {
                if (!plugin.getDescription().getVersion().equals(version)) {
                    String prefix = plugin.getConfig().getString("Prefix");
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "" + ChatColor.YELLOW + "There is a new update available for Nessentials. You are on " + ChatColor.RED + plugin.getDescription().getVersion() + ChatColor.YELLOW + " Latest version is " + ChatColor.GREEN + version + ChatColor.YELLOW + "!" + "\nGo to " + ChatColor.RESET + "https://www.spigotmc.org/resources/nessentials.112117/" + ChatColor.YELLOW + " to download the latest version.");
                }
            });
        }

        if (p.hasPermission("nessentials.*")) {
            timerUtil.severeMessage(p);
            timerUtil.warningMessage(p);
        }

    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        String leave = this.plugin.getConfig().getString("Leave-message");
        Player p = e.getPlayer();

        if (leave != ""){
            if (vanishedPlayers.contains(p.getUniqueId())) {
                e.setQuitMessage(null);
            }else {
                leave = leave.replace("[player]", e.getPlayer().getDisplayName());
                leave = leave.replace("[server_name]", plugin.getConfig().getString("Server_Name"));
                e.setQuitMessage(ChatColor.translateAlternateColorCodes('&', leave));
            }
        }else {
            if (vanishedPlayers.contains(p.getUniqueId())) {
                e.setQuitMessage(null);
            }
        }

        if (plugin.incomingRequest2.get(p.getUniqueId()) != null) {
            Player sender = Bukkit.getPlayer(plugin.incomingRequest2.get(p.getUniqueId()));
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + p.getDisplayName() + ChatColor.RED + " Left the game so tpa request canceled!");
            sender.performCommand("tpac");
        }

        if (plugin.outGoingRequest.get(p.getUniqueId()) != null) {
            Player target = Bukkit.getPlayer(plugin.incomingRequest.get(p.getUniqueId()));
            plugin.incomingRequest.put(p.getUniqueId(), null);
            plugin.incomingRequest2.put(target.getUniqueId(), null);
            plugin.outGoingRequest.put(p.getUniqueId(), null);
            plugin.tpaHere.put(p.getUniqueId(), null);
            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + p.getDisplayName() + ChatColor.RED + " Left the game so tpa canceled!");
        }

    }
}
