package ml.mcpland.nin1275.nessentials.listeners;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerCommandSendEvent;

public class messageListener implements Listener {
    private final Nessentials plugin;

    public messageListener(Nessentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        String msg = e.getMessage();
        if (msg.contains("/discord")) {
            if (!plugin.getConfig().getBoolean("Discord-Command.Enabled")) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onCommandTab(PlayerCommandSendEvent e) {
        if (!plugin.getConfig().getBoolean("Discord-Command.Enabled")) {
            e.getCommands().remove("discord");
        }
    }
}
