package ml.mcpland.nin1275.nessentials.listeners;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Objects;

import static ml.mcpland.nin1275.nessentials.commands.vanishCommand.vanishedPlayers;

public class vanishListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        if (vanishedPlayers.contains(p.getUniqueId())) {
            for (Player otherPlayer : Bukkit.getOnlinePlayers()) {
                if (otherPlayer.equals(p)) continue;
                if (vanishedPlayers.contains(otherPlayer.getUniqueId())) {
                    otherPlayer.showPlayer(p);
                    p.showPlayer(otherPlayer);
                } else {
                    otherPlayer.hidePlayer(p);
                }
            }
        }else {
            for (Player otherPlayer : Bukkit.getOnlinePlayers()) {
                if (otherPlayer.equals(p)) {
                    continue;
                }
                if (vanishedPlayers.contains(otherPlayer.getUniqueId())) {
                    p.hidePlayer(otherPlayer);
                }
                otherPlayer.showPlayer(p);
            }
        }
    }
    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getPlayer();

        if (vanishedPlayers.contains(p.getUniqueId())) {
            e.setDeathMessage(null);
        }
    }
}