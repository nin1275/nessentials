package ml.mcpland.nin1275.nessentials.listeners;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.files.dataConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.server.ServerListPingEvent;

public class maintenanceListener implements Listener {
    private final Nessentials plugin;

    public maintenanceListener(Nessentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPing(ServerListPingEvent e) {
        String motd = Bukkit.getMotd();
        if(dataConfig.getConfig().getBoolean("maintenance")) {
            e.setMotd(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Maintenance-MOTD")));
        }else {
            if (!dataConfig.getConfig().getBoolean("maintenance")) {
                e.setMotd(ChatColor.translateAlternateColorCodes('&', motd));
            }
        }
    }


    @EventHandler
    public void maintenanceJoin(PlayerLoginEvent e) {
        if(!e.getPlayer().hasPermission("nessentials.maintanence.bypass") && dataConfig.getConfig().getBoolean("maintenance")) {
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Kick-Message")));
        }
    }
}
