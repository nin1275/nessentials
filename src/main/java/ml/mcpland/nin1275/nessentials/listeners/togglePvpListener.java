package ml.mcpland.nin1275.nessentials.listeners;

import ml.mcpland.nin1275.nessentials.files.dataConfig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class togglePvpListener implements Listener {
    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            if (e.getEntity() instanceof Player) {
                if (!dataConfig.getConfig().getBoolean("pvp")) {
                    e.setCancelled(true);
                }
            }
        }
    }
}
