package ml.mcpland.nin1275.nessentials.listeners;

import ml.mcpland.nin1275.nessentials.Nessentials;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.*;
import org.bukkit.scoreboard.Scoreboard;
import org.jetbrains.annotations.NotNull;


public class HealthUnderName implements Listener {

    private final Nessentials plugin;

    public HealthUnderName(Nessentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (plugin.getConfig().getBoolean("Health_Under-Name")) {
            Player p = event.getPlayer();
            Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective objective = board.registerNewObjective(
                "_health", Criterias.HEALTH, ChatColor.RED + "❤");
            objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
            p.setScoreboard(board);
        }
    }
}
