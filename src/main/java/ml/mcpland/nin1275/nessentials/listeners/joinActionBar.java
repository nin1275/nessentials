package ml.mcpland.nin1275.nessentials.listeners;

import ml.mcpland.nin1275.nessentials.Nessentials;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class joinActionBar implements Listener {

    private final Nessentials plugin;


    public joinActionBar(Nessentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        String msg = this.plugin.getConfig().getString("Join-ActionBar-Message");
        String title = this.plugin.getConfig().getString("Join_Title");
        String subtitle = this.plugin.getConfig().getString("Join_SubTitle");
        Boolean sound = this.plugin.getConfig().getBoolean("Join-Sound");

        if (msg != "") {
            msg = msg.replace("[player]", e.getPlayer().getDisplayName());
            msg = msg.replace("[server_name]", plugin.getConfig().getString("Server_Name"));
            e.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.translateAlternateColorCodes('&', msg)));
        }

        if (sound != false){
            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
        }

        if (title != "") {
            title = title.replace("[player]", e.getPlayer().getDisplayName());
            title = title.replace("[server_name]", plugin.getConfig().getString("Server_Name"));
            subtitle = subtitle.replace("[player]", e.getPlayer().getDisplayName());
            subtitle = subtitle.replace("[server_name]", plugin.getConfig().getString("Server_Name"));
            e.getPlayer().sendTitle(ChatColor.translateAlternateColorCodes('&', title), ChatColor.translateAlternateColorCodes('&', subtitle), 1, 20, 1);
        }
    }
}