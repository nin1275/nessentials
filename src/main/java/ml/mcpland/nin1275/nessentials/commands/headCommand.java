package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.jetbrains.annotations.NotNull;

public class headCommand implements CommandExecutor {

    private final Nessentials plugin;

    public headCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (p.hasPermission("nessentials.head") || p.hasPermission("nessentials.*")){

                if (args.length == 0){
                    p.getInventory().addItem(getPlayerHead(p.getName()));
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GOLD + "You were given " + ChatColor.RED + p.getDisplayName() + ChatColor.WHITE + "'s " + ChatColor.GOLD + "Head");
                }else{
                    p.getInventory().addItem(getPlayerHead(args[0]));
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GOLD + "You were given " + ChatColor.RED + args[0] + ChatColor.WHITE + "'s " + ChatColor.GOLD + "Head");
                }
            }else{
                String noperm = this.plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }

        }else{
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must be a Player to execute this command!");
        }
        return true;
    }

    public ItemStack getPlayerHead(String p){
        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        meta.setOwner(p);
        item.setItemMeta(meta);
        return item;
    }
}
