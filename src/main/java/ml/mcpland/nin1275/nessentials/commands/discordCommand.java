package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class discordCommand implements CommandExecutor {
    private final Nessentials plugin;

    public discordCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (plugin.getConfig().getString("Discord-Command.Message") != null) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Discord-Command.Message")));
            }else {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Set the Discord server link in Config.yml");
            }
        }
        return true;
    }
}
