package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.files.dataConfig;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class togglePVPcommand implements TabExecutor {

    private final Nessentials plugin;

    public togglePVPcommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("nessentials.pvpCommand") || p.hasPermission("nessentials.*") || p.hasPermission("nessentials.staff") || p.hasPermission("nessentials.admin")) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("on")) {
                        dataConfig.getConfig().set("pvp", true);
                        dataConfig.save();
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Enabled");
                        return true;
                    }
                    if (args[0].equalsIgnoreCase("off")) {
                        dataConfig.getConfig().set("pvp", false);
                        dataConfig.save();
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Disabled");
                        return true;
                    }
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage. Please select between \"on\" or \"off\".");
                }else {
                    if (args.length < 1) {
                        if (dataConfig.getConfig().getBoolean("pvp")) {
                            dataConfig.getConfig().set("pvp", false);
                            dataConfig.save();
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Disabled");
                        }else if (!dataConfig.getConfig().getBoolean("pvp")) {
                            dataConfig.getConfig().set("pvp", true);
                            dataConfig.save();
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Enabled");
                        }
                        return true;
                    }
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage. Please select between \"on\" or \"off\".");
                }
            }else {
                String noperm = plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }
        }else {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("on")) {
                    dataConfig.getConfig().set("pvp", true);
                    dataConfig.save();
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Enabled");
                    return true;
                }
                if (args[0].equalsIgnoreCase("off")) {
                    dataConfig.getConfig().set("pvp", false);
                    dataConfig.save();
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Disabled");
                    return true;
                }
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage. Please select between \"on\" or \"off\".");
            }else {
                if (args.length < 1) {
                    if (dataConfig.getConfig().getBoolean("pvp")) {
                        dataConfig.getConfig().set("pvp", false);
                        dataConfig.save();
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Disabled");
                    }else if (!dataConfig.getConfig().getBoolean("pvp")) {
                        dataConfig.getConfig().set("pvp", true);
                        dataConfig.save();
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "PVP Enabled");
                    }
                    return true;
                }
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage. Please select between \"on\" or \"off\".");
            }
        }

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("on");
            arguments.add("off");
            return arguments;
        }
        return null;
    }
}
