package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class flyCommand implements CommandExecutor {

    private final Nessentials plugin;

    public flyCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        Player p = (Player) sender;
        if (sender instanceof Player) {
            if (p.hasPermission("nessentials.fly") || p.hasPermission("nessentials.staff") || p.hasPermission("nessentials.admin") || p.hasPermission("nessentials.*")) {
                if (args.length == 1) {
                    String playerName = args[0];
                    Player target = Bukkit.getServer().getPlayerExact(playerName);

                    if (target == null) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player is not online!");
                    } else {
                        if (target.getAllowFlight()) {
                            target.setAllowFlight(false);
                            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + p.getDisplayName() + ChatColor.RED + " Disabled your Flight.");
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully set " + ChatColor.DARK_RED + target.getDisplayName() + "'s" + ChatColor.GREEN + " Flight Off.");
                        } else {
                            target.setAllowFlight(true);
                            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + p.getDisplayName() + ChatColor.GREEN + " Allowed you to Fly.");
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully set " + ChatColor.DARK_RED + target.getDisplayName() + "'s" + ChatColor.GREEN + " Flight On.");
                        }
                    }
                } else {
                    if (p.getAllowFlight()) {
                        p.setAllowFlight(false);
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Flight disabled!");
                    } else {
                        p.setAllowFlight(true);
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Flight enabled!");
                    }
                }
            }else {
                String noperm = this.plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }
        }else {
            if (args.length == 1) {
                String playerName = args[0];
                Player target = Bukkit.getServer().getPlayerExact(playerName);

                if (target == null) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player is not online!");
                }else {
                    if (target.getAllowFlight()) {
                        target.setAllowFlight(false);
                        target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "CONSOLE" + ChatColor.RED + " Disabled your Flight.");
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully set " + ChatColor.DARK_RED + target.getDisplayName() + "'s" + ChatColor.GREEN + " Flight Off.");
                    } else {
                        target.setAllowFlight(true);
                        target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "CONSOLE" + ChatColor.GREEN + " Allowed you to Fly.");
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully set " + ChatColor.DARK_RED + target.getDisplayName() + "'s" + ChatColor.GREEN + " Flight On.");
                    }
                }
            }else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must put a player name!");
            }
        }
        return true;
    }
}
