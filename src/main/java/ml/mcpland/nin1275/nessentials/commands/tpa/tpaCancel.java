package ml.mcpland.nin1275.nessentials.commands.tpa;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class tpaCancel implements CommandExecutor {
    private final Nessentials plugin;

    public tpaCancel(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            Player target = Bukkit.getPlayer(plugin.incomingRequest.get(p.getUniqueId()));
            if (plugin.outGoingRequest.get(p.getUniqueId()) != null) {
                plugin.outGoingRequest.put(p.getUniqueId(), null);
                plugin.incomingRequest2.put(target.getUniqueId(), null);
                plugin.incomingRequest.put(p.getUniqueId(), null);
                plugin.tpaHere.put(p.getUniqueId(), null);
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Canceled the Request.");
                target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + p.getDisplayName() + ChatColor.RED + " Canceled the request.");
            }else {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "You didn't send a tpa request.");
                return true;
            }
        }else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Must be a Player to execute this command!");
            return true;
        }
        return true;
    }
}
