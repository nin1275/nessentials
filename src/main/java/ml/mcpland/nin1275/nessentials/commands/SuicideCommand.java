package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SuicideCommand implements CommandExecutor {
    private final Nessentials plugin;

    public SuicideCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    // /suicide <playerName>

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if(sender instanceof Player p){
            if(args.length == 0){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "You Committed suicide :c");
                p.setHealth(0.0);
            }else {
                if (p.hasPermission("nessentials.suicide") || p.hasPermission("nessentials.*")) {
                    String playerName = args[0];
                    Player target = Bukkit.getServer().getPlayerExact(playerName);

                    if (target == null) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player is not online!");
                    } else {

                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "You successfully made " + ChatColor.RED + target.getDisplayName() + "" + ChatColor.RESET + " Commit " + ChatColor.RED + "Suicide!");
                        target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "You just Committed " + ChatColor.RED + "Suicide " + ChatColor.RESET + "by " + ChatColor.RED + p.getDisplayName() + "!");
                        target.setHealth(0.0);

                    }

                }else{
                    String noperm = this.plugin.getConfig().getString("No-Perm");
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
                }
            }
        }else{
            if(args.length == 0){
                sender.sendMessage(ChatColor.RED + "Must put a player name!");
            }else{
                String playerName = args[0];
                Player target = Bukkit.getServer().getPlayerExact(playerName);

                if (target == null) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player is not online!");
                } else {

                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "You successfully made " + ChatColor.RED + target.getDisplayName() + "" + ChatColor.RESET + " Commit " + ChatColor.RED + "Suicide!");
                    target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "You just Committed " + ChatColor.RED + "Suicide " + ChatColor.RESET + "by " + ChatColor.RED + "CONSOLE" + "!");
                    target.setHealth(0.0);

                }
            }
        }

        return true;
    }

}