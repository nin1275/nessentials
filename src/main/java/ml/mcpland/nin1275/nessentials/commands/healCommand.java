package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class healCommand implements CommandExecutor {

    private final Nessentials plugin;

    public healCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(p.hasPermission("nessentials.heal") || p.hasPermission("nessentials.*")) {
                if(args.length == 0) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§aYou have been successfully healed!");
                    p.setFoodLevel(20);
                    p.setHealth(p.getMaxHealth());


                } else if(args.length == 1 ) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if(target != null) {
                        target.setHealth(target.getMaxHealth());
                        target.setFoodLevel(20);
                        target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§aYou were healed by " + ChatColor.RED + p.getDisplayName() + ChatColor.GREEN + "!");
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§aYou healed the Player §c" + target.getDisplayName());
                    } else {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§cThe player §4" + args[0] + " §cis not on the server!");
                    }
                } else {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§cPlease use §6/heal <Player>§c!");
                }


            } else {
                String noperm = this.plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }

        } else {
            if (args.length == 0){
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must put a player name!");
            }else if(args.length == 1 ) {
                Player target = Bukkit.getPlayer(args[0]);
                if(target != null) {
                    target.setHealth(target.getMaxHealth());
                    target.setFoodLevel(20);
                    target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§aYou were healed by " + ChatColor.RED + "CONSOLE" + ChatColor.GREEN + "!");
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§aYou healed the Player §c" + target.getDisplayName());
                } else {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§cThe player §4" + args[0] + " §cis not on the server!");
                }
            } else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "§cPlease use §6/heal <Player>§c!");
            }
        }



        return true;
    }

}