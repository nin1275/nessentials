package ml.mcpland.nin1275.nessentials.commands.tpa;

import ml.mcpland.nin1275.nessentials.Nessentials;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class tpaCommand implements CommandExecutor {

    //TODO: cancel send if the target has a request sent to them from someone else

    private final Nessentials plugin;

    public tpaCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        TextComponent am = new TextComponent(ChatColor.GREEN + "[Accept] ");
        am.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpaa")); // tpaa
        am.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Click to accept or do /tpaaccept")));
        TextComponent dm = new TextComponent(ChatColor.RED + "[Deny]");
        dm.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpad")); // tpad
        dm.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Click to deny or do /tpadeny")));
        TextComponent pm = new TextComponent(ChatColor.translateAlternateColorCodes('&', prefix));

        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (plugin.outGoingRequest.get(p.getUniqueId()) != null) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "You already have a tpa request! Please cancel and try again.");
                return true;
            }else {
                if (args.length == 1) {
                    String playerName = args[0];
                    Player target = Bukkit.getServer().getPlayerExact(playerName);
                    if (plugin.incomingRequest2.get(target.getUniqueId()) == null) {
                        if (target != null) {
                            if (target.getUniqueId() != p.getUniqueId()) {
                                // code the request
                                plugin.outGoingRequest.put(p.getUniqueId(), true);
                                plugin.incomingRequest.put(p.getUniqueId(), target.getUniqueId());
                                plugin.incomingRequest2.put(target.getUniqueId(), p.getUniqueId());
                                target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + p.getDisplayName() + ChatColor.GREEN + " Sent you a " + ChatColor.GOLD + "TPA " + ChatColor.WHITE + "request.");
                                target.spigot().sendMessage(pm, am, dm);
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Sent tpa request. " + ChatColor.RED + "Expiring in " + ChatColor.WHITE + "1 minute!");
                                return true;
                            }else {
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "You can't tpa yourself!");
                                return true;
                            }
                        }else {
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player is not online!");
                            return true;
                        }
                    }else {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player already has a request sent to them!");
                        return true;
                    }
                }
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/tpa [player]");
                return true;
            }
        }else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must be a Player to execute this command!");
            return true;
        }
    }
}
