package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.files.dataConfig;
import ml.mcpland.nin1275.nessentials.files.infoConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class mainCommand implements TabExecutor {

    private final Nessentials plugin;

    public mainCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (sender instanceof Player){
            Player p = ((Player) sender).getPlayer();

            if(args.length == 0){
                p.sendMessage(ChatColor.YELLOW + "Running " + ChatColor.GOLD + "NESSENTIALS" + ChatColor.YELLOW + " v" + this.plugin.getDescription().getVersion());
                p.sendMessage(ChatColor.YELLOW + "Plugin Made by: " + ChatColor.RED + "NIN1275");
                p.sendMessage(ChatColor.YELLOW + "Do " + ChatColor.AQUA + "/nessentials help" + ChatColor.YELLOW + " for a list of commands.");
                return true;
            }else {
                if (args[0].equalsIgnoreCase("help")) {
                    if (args.length >= 2){
                        p.performCommand("? nessentials " + args[1]);
                    }else {
                        p.performCommand("? nessentials");
                    }
                    return true;
                }

                if (args[0].equalsIgnoreCase("reload")) {
                    if (p.hasPermission("nessentials.reload")) {
                        plugin.reloadConfig();
                        dataConfig.reload();
                        infoConfig.reload();

                        // home and warps max cap and least cap saftey check
                        if (plugin.getConfig().get("Homes.Max-homes") == null) {
                            plugin.getConfig().set("Homes.Max-homes", 5);
                            plugin.saveConfig();
                        }

                        if (plugin.getConfig().get("Warps.Max-warps") == null) {
                            plugin.getConfig().set("Warps.Max-warps", 5);
                            plugin.saveConfig();
                        }

                        if ((int) plugin.getConfig().get("Homes.Max-homes") > 50) {
                            plugin.getConfig().set("Homes.Max-homes", 50);
                            plugin.saveConfig();
                        }

                        if ((int) plugin.getConfig().get("Homes.Max-homes") < 0) {
                            plugin.getConfig().set("Homes.Max-homes", 0);
                            plugin.saveConfig();
                        }

                        if ((int) plugin.getConfig().get("Warps.Max-warps") > 50) {
                            plugin.getConfig().set("Warps.Max-warps", 50);
                            plugin.saveConfig();
                        }

                        if ((int) plugin.getConfig().get("Warps.Max-warps") < 0) {
                            plugin.getConfig().set("Warps.Max-warps", 0);
                            plugin.saveConfig();
                        }

                        String prefix = this.plugin.getConfig().getString("Prefix");
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "Plugin Config reloaded.");
                        return true;
                    } else {
                        String noperm = plugin.getConfig().getString("No-Perm");
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
                    }
                }
            }
        }else {
            if(args.length == 0){
                sender.sendMessage(ChatColor.YELLOW + "Running " + ChatColor.GOLD + "NESSENTIALS");
                sender.sendMessage(ChatColor.YELLOW + "Plugin Made by: " + ChatColor.RED + "NIN1275");
                sender.sendMessage(ChatColor.YELLOW + "Do " + ChatColor.AQUA + "/nessentials help" + ChatColor.YELLOW + " for a list of commands.");
                return true;
            }else {
                if (args[0].equalsIgnoreCase("help")) {
                    if (args.length >= 2){
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "? nessentials " + args[1]);
                    }else {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "? nessentials");
                    }
                    return true;
                }

                if (args[0].equalsIgnoreCase("reload")) {
                    plugin.reloadConfig();
                    dataConfig.reload();
                    infoConfig.reload();

                    // home and warps max cap and least cap saftey check
                    if (plugin.getConfig().get("Homes.Max-homes") == null) {
                        plugin.getConfig().set("Homes.Max-homes", 5);
                        plugin.saveConfig();
                    }

                    if (plugin.getConfig().get("Warps.Max-warps") == null) {
                        plugin.getConfig().set("Warps.Max-warps", 5);
                        plugin.saveConfig();
                    }

                    if ((int) plugin.getConfig().get("Homes.Max-homes") > 50) {
                        plugin.getConfig().set("Homes.Max-homes", 50);
                        plugin.saveConfig();
                    }

                    if ((int) plugin.getConfig().get("Homes.Max-homes") < 0) {
                        plugin.getConfig().set("Homes.Max-homes", 0);
                        plugin.saveConfig();
                    }

                    if ((int) plugin.getConfig().get("Warps.Max-warps") > 50) {
                        plugin.getConfig().set("Warps.Max-warps", 50);
                        plugin.saveConfig();
                    }

                    if ((int) plugin.getConfig().get("Warps.Max-warps") < 0) {
                        plugin.getConfig().set("Warps.Max-warps", 0);
                        plugin.saveConfig();
                    }

                    String prefix = this.plugin.getConfig().getString("Prefix");
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "Plugin Config reloaded.");
                    return true;
                }
            }
        }
        return true;
    }


    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1) {
            List<String> arguments = new ArrayList<>();
            arguments.add("help");
            arguments.add("reload");
            return arguments;
        }
        if (args[0].equalsIgnoreCase("help")) {
            List<String> arguments = new ArrayList<>();
            arguments.add("1");
            arguments.add("2");
            arguments.add("3");
            arguments.add("4");
            arguments.add("5");
            arguments.add("6");
            return arguments;
        }
        // arguments.add(" ");
        return null;
    }
}
