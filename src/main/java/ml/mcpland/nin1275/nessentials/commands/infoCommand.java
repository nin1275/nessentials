package ml.mcpland.nin1275.nessentials.commands;

import com.google.gson.JsonObject;
import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.stuff.Metrics;
import ml.mcpland.nin1275.nessentials.stuff.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

public class infoCommand implements CommandExecutor {

    private final Nessentials plugin;

    public infoCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("nessentials.info") || p.hasPermission("nessentials.*") || p.hasPermission("nessentials.staff") || p.hasPermission("nessentials.admin")) {
                if (args.length == 1) {
                    String playerName = args[0];
                    Player target = Bukkit.getServer().getPlayerExact(playerName);
                    if (target != null) {
                        p.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                        p.sendMessage(ChatColor.WHITE + "Player" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getName());
                        p.sendMessage(ChatColor.WHITE + "UUID" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getUniqueId());
                        if (target.isBanned()) {
                            p.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                        }else {
                            p.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                        }
                        p.sendMessage(" ");
                        if (target.hasPlayedBefore()) {
                            p.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                            p.sendMessage(ChatColor.WHITE + "Last login" + ChatColor.GRAY + ": " + ChatColor.AQUA + Date.from(Instant.ofEpochSecond(target.getLastLogin())));
                            p.sendMessage(ChatColor.WHITE + "First Join" + ChatColor.GRAY + ": " + ChatColor.AQUA + Date.from(Instant.ofEpochSecond(target.getFirstPlayed())));
                        }else {
                            p.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                        }
                        p.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                    }else {
                        String uf = UUIDFetcher.getJSON("https://api.minecraftservices.com/minecraft/profile/lookup/name/" + playerName);
                        p.sendMessage(uf);
                        if (uf != null) {
                            target = Bukkit.getOfflinePlayer(uf).getPlayer();
                            p.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                            p.sendMessage(ChatColor.WHITE + "Player" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getName());
                            p.sendMessage(ChatColor.WHITE + "UUID" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getUniqueId());
                            if (target.isBanned()) {
                                p.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                            }else {
                                p.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                            }
                            p.sendMessage(" ");
                            if (target.hasPlayedBefore()) {
                                p.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                                p.sendMessage(ChatColor.WHITE + "Last login" + ChatColor.GRAY + ": " + ChatColor.AQUA + Date.from(Instant.ofEpochSecond(target.getLastLogin())));
                                p.sendMessage(ChatColor.WHITE + "First Join" + ChatColor.GRAY + ": " + ChatColor.AQUA + Date.from(Instant.ofEpochSecond(target.getFirstPlayed())));
                            }else {
                                p.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                            }
                            p.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                        }else {
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player does not exist!");
                        }
                    }
                }else {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must specify a player!");
                }
            }else {
                String noperm = plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }
        }else {
            if (args.length == 1) {
                String playerName = args[0];
                Player target = Bukkit.getServer().getPlayerExact(playerName);
                if (target != null) {
                    sender.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                    sender.sendMessage(ChatColor.WHITE + "Player" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getName());
                    sender.sendMessage(ChatColor.WHITE + "UUID" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getUniqueId());
                    if (target.isBanned()) {
                        sender.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                    }else {
                        sender.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                    }
                    sender.sendMessage(" ");
                    if (target.hasPlayedBefore()) {
                        sender.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                        sender.sendMessage(ChatColor.WHITE + "Last login" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getLastLogin());
                        sender.sendMessage(ChatColor.WHITE + "First Join" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getFirstPlayed());
                    }else {
                        sender.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                    }
                    sender.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                }else {
                    target = (Player) Bukkit.getServer().getOfflinePlayer(playerName);
                    if (target != null) {
                        sender.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                        sender.sendMessage(ChatColor.WHITE + "Player" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getName());
                        sender.sendMessage(ChatColor.WHITE + "UUID" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getUniqueId());
                        if (target.isBanned()) {
                            sender.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                        }else {
                            sender.sendMessage(ChatColor.WHITE + "Banned" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                        }
                        sender.sendMessage(" ");
                        if (target.hasPlayedBefore()) {
                            sender.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.GREEN + "True");
                            sender.sendMessage(ChatColor.WHITE + "Last login" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getLastLogin());
                            sender.sendMessage(ChatColor.WHITE + "First Join" + ChatColor.GRAY + ": " + ChatColor.AQUA + target.getFirstPlayed());
                        }else {
                            sender.sendMessage(ChatColor.WHITE + "Joined Before" + ChatColor.GRAY + ": " + ChatColor.RED + "False");
                        }
                        sender.sendMessage(ChatColor.GRAY + "----------------" + ChatColor.RED + " nessentials " + ChatColor.GRAY + "----------------");
                    }else {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player does not exist!");
                    }
                }
            }else {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must specify a player!");
            }
        }
        return true;
    }
}
