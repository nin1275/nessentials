package ml.mcpland.nin1275.nessentials.commands.warps;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.files.infoConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class setWarp implements TabExecutor {
    private final Nessentials plugin;

    public setWarp(Nessentials plugin) {
        this.plugin = plugin;
    }
    private boolean found = false;
    private int max2 = 0;

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        Integer max = (Integer) plugin.getConfig().get("Warps.Max-warps");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            String w = p.getWorld().getName();
            double x = p.getLocation().getX();
            double y = p.getLocation().getY();
            double z = p.getLocation().getZ();
            Integer warps = (Integer) infoConfig.getConfig().get("Warps." + p.getUniqueId() + ".warps");
            if (p.hasPermission("nessentials.*") || p.hasPermission("nessentials.setwarp") || p.hasPermission("nessentials.admin") || p.hasPermission("nessentials.staff")) {
                if (infoConfig.getConfig().get("Warps." + p.getUniqueId() + ".warps") != null) {
                    if (warps != null) {
                        if (warps >= max) {
                            Bukkit.broadcastMessage("e1");
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "You have the max amount of warps!");
                            return true;
                        }else {
                            Bukkit.broadcastMessage("m1");
                            warps = warps + 1;
                            infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warps", warps);
                            infoConfig.save();
                            if (args.length == 1) {
                                String name = args[0];
                                while (!found) {
                                    while (max2 <= 50) {
                                        max2 = max2 + 1;
                                        if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max2 + ".name") != null) {
                                            if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max2 + ".name").equalsIgnoreCase(name)) {
                                                found = true;
                                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Warp already exists!");
                                                max2 = 0;
                                                found = false;
                                                return true;
                                            }
                                        }
                                    }
                                    infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".name", name);
                                    infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.w", w);
                                    infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.x", x);
                                    infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.y", y);
                                    infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.z", z);
                                    infoConfig.save();
                                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved warp, " + ChatColor.RED + name);
                                    return true;
                                }
                            }else if (args.length == 0) {
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/setwarp [warp-name]");
                                return true;
                            }
                        }
                    }else {
                        infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warps", warps);
                        infoConfig.save();
                        if (args.length == 1) {
                            Bukkit.broadcastMessage("m2");
                            String name = args[0];
                            while (!found) {
                                while (max2 <= 50) {
                                    max2 = max2 + 1;
                                    if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max2 + ".name") != null) {
                                        if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max2 + ".name").equalsIgnoreCase(name)) {
                                            found = true;
                                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Warp already exists!");
                                            max2 = 0;
                                            found = false;
                                            return true;
                                        }
                                    }
                                }
                                infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".name", name);
                                infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.w", w);
                                infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.x", x);
                                infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.y", y);
                                infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.z", z);
                                infoConfig.save();
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved warp, " + ChatColor.RED + name);
                                return true;
                            }
                        } else if (args.length == 0) {
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/setwarp [warp-name]");
                            return true;
                        }
                    }
                }else {
                    warps = 1;
                    infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".ign", p.getName());
                    infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warps", warps);
                    infoConfig.save();
                    if (args.length == 1) {
                        Bukkit.broadcastMessage("m4");
                        String name = args[0];
                        while (!found) {
                            while (max2 <= 50) {
                                max2 = max2 + 1;
                                if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max2 + ".name") != null) {
                                    if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max2 + ".name").equalsIgnoreCase(name)) {
                                        found = true;
                                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Warp already exists!");
                                        max2 = 0;
                                        found = false;
                                        return true;
                                    }
                                }
                            }
                            infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".name", name);
                            infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.w", w);
                            infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.x", x);
                            infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.y", y);
                            infoConfig.getConfig().set("Warps." + p.getUniqueId() + ".warp" + warps + ".loc.z", z);
                            infoConfig.save();
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved warp, " + ChatColor.RED + name);
                            return true;
                        }
                    } else if (args.length == 0) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/setwarp [warp-name]");
                        return true;
                    }
                }
            }
        }else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must be a Player to execute this command!");
        }
        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("<warp-name>");
            return arguments;
        }
        return null;
    }
}
