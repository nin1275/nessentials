package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class godmodeCommand implements CommandExecutor {

    private final Nessentials plugin;

    public godmodeCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(p.hasPermission("nessentials.godmode") || p.hasPermission("nessentials.staff") || p.hasPermission("nessentials.*")){

                if (args.length == 0){
                    if (p.isInvulnerable()){
                        p.setInvulnerable(false);
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Godmode Disabled!");
                    }else{
                        p.setInvulnerable(true);
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Godmode Enabled!");
                    }
                }

                if (args.length == 1){
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null){
                        if (target.isInvulnerable()){
                            target.setInvulnerable(false);
                            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Godmode Disabled by " + p.getDisplayName());
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "You Disabled " + ChatColor.RED + target.getDisplayName() + ChatColor.GREEN + " Godmode!");
                        }else{
                            target.setInvulnerable(true);
                            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Godmode Enabled by " + ChatColor.RED + p.getDisplayName());
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "You Enabled " + ChatColor.RED + target.getDisplayName() + ChatColor.GREEN + " Godmode!");
                        }
                    }else{
                        p.sendMessage("§cThe player §4" + args[0] + " §cis not on the server!");
                    }
                }

            }else{
                String noperm = this.plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }
        }else{
            if (args.length == 0){
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must put a player name!");
            }else{
                if (args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0]);
                    if (target != null) {
                        if (target.isInvulnerable()) {
                            target.setInvulnerable(false);
                            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Godmode Disabled by " + ChatColor.RED + "CONSOLE");
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "You Disabled " + ChatColor.RED + target.getDisplayName() + ChatColor.GREEN + " Godmode!");
                        } else {
                            target.setInvulnerable(true);
                            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Godmode Enabled by " + ChatColor.RED + "CONSOLE");
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "You Enabled " + ChatColor.RED + target.getDisplayName() + ChatColor.GREEN + " Godmode!");
                        }
                    } else {
                        sender.sendMessage("§cThe player §4" + args[0] + " §cis not on the server!");
                    }
                }
            }
        }
        return true;
    }
}
