package ml.mcpland.nin1275.nessentials.commands.tpa;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.stuff.timerUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class tpaAccept implements CommandExecutor {
    private final Nessentials plugin;

    public tpaAccept(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String str, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (plugin.incomingRequest2.get(p.getUniqueId()) != null) {
                Player s = Bukkit.getPlayer(plugin.incomingRequest2.get(p.getUniqueId()));
                plugin.incomingRequest.put(s.getUniqueId(), null);
                plugin.outGoingRequest.put(s.getUniqueId(), null);
                plugin.incomingRequest2.put(p.getUniqueId(), null);
                plugin.tpaHere.put(s.getUniqueId(), null);
                if (plugin.tpaHere.get(s.getUniqueId()) != null) {
                    Bukkit.broadcastMessage("h");
                    timerUtil.tpaHere(p, s);
                }else {
                    Bukkit.broadcastMessage("t");
                    timerUtil.tpa(p, s);
                }
                s.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + p.getDisplayName() + ChatColor.GREEN + " Accepted your tpa request!");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Accepted " + ChatColor.RED + s.getDisplayName() + ChatColor.GREEN + " tpa request.");
            }else {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "You don't have a tpa request.");
                return true;
            }
        }else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Must be a Player to execute this command!");
            return true;
        }
        return true;
    }
}
