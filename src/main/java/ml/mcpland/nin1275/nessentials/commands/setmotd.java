package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class setmotd implements TabExecutor{

    private final Nessentials plugin;

    public setmotd(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("nessentials.setmotd") || p.hasPermission("nessentials.admin") || p.hasPermission("nessentials.*")) {

                if (args.length > 0){
                    StringBuilder motd = new StringBuilder();
                    for (int i = 0; i < args.length; i++){
                        motd.append(args[i] + " ");
                    }
                    plugin.getConfig().set("motd", motd.toString());
                    plugin.saveConfig();
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "MOTD Successfully was set too... " + motd);
                }else{
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage! " + ChatColor.RESET + "Do " + ChatColor.AQUA + "/setmotd text");
                }
            } else {
                String noperm = plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }
        } else {

            if (args.length > 0) {
                StringBuilder motd = new StringBuilder();
                for (int i = 0; i < args.length; i++) {
                    motd.append(args[i] + " ");
                }
                plugin.getConfig().set("motd", motd.toString());
                plugin.saveConfig();
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "MOTD Successfully was set too... " + ChatColor.RESET + motd);
            }else{
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage! " + ChatColor.RESET + "Do " + ChatColor.AQUA + "/setmotd text");
            }
    }
        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (args.length > 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("");
//            arguments.add(" ");

            return arguments;
        }

        if (args.length == 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("");
//            arguments.add(" ");

            return arguments;
        }

        return null;
    }
}
