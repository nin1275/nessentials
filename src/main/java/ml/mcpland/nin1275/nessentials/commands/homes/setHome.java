package ml.mcpland.nin1275.nessentials.commands.homes;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.files.infoConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class setHome implements TabExecutor {
    private final Nessentials plugin;

    public setHome(Nessentials plugin) {
        this.plugin = plugin;
    }
    private boolean found = false;
    private int max2 = 0;

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        int max = plugin.getConfig().getInt("Homes.Max-homes");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            String w = p.getWorld().getName();
            double x = p.getLocation().getX();
            double y = p.getLocation().getY();
            double z = p.getLocation().getZ();
            Integer homes = infoConfig.getConfig().getInt("Homes." + p.getUniqueId() + ".homes");
            if (p.hasPermission("nessentials.*") || p.hasPermission("nessentials.sethome") || p.hasPermission("nessentials.admin") || p.hasPermission("nessentials.staff")) {
                if (infoConfig.getConfig().get("Homes." + p.getUniqueId() + ".homes") != null) {
                    if (homes != null) {
                        if (homes >= max) {
                            Bukkit.broadcastMessage("e1");
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "You have the max amount of homes!");
                            return true;
                        }else {
                            Bukkit.broadcastMessage("m1");
                            homes = homes + 1;
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".homes", homes);
                            infoConfig.save();
                            if (args.length == 1) {
                                String name = args[0];
                                while (!found) {
                                    while (max2 <= 50) {
                                        max2 = max2 + 1;
                                        if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home" + max2 + ".name") != null) {
                                            if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home" + max2 + ".name").equalsIgnoreCase(name)) {
                                                found = true;
                                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Home already exists!");
                                                max2 = 0;
                                                found = false;
                                                return true;
                                            }
                                        }
                                    }
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".name", name);
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.w", w);
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.x", x);
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.y", y);
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.z", z);
                                    infoConfig.save();
                                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved home, " + ChatColor.RED + name);
                                    return true;
                                }
                            }else if (args.length == 0) {
                                if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home1.name") != null) {
                                    Bukkit.broadcastMessage("m2");
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".name", "home");
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.w", w);
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.x", x);
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.y", y);
                                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.z", z);
                                    infoConfig.save();
                                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved home, " + ChatColor.RED + "home");
                                    return true;
                                }else {
                                    Bukkit.broadcastMessage("e2");
                                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Home already exists try specifying a name!");
                                    return true;
                                }
                            }
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/sethome [home-name]");
                            return true;
                        }
                    }else {
                        infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".homes", homes);
                        infoConfig.save();
                        if (args.length == 1) {
                            Bukkit.broadcastMessage("m3");
                            String name = args[0];
                            while (!found) {
                                while (max2 <= 50) {
                                    max2 = max2 + 1;
                                    if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home" + max2 + ".name") != null) {
                                        if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home" + max2 + ".name").equalsIgnoreCase(name)) {
                                            found = true;
                                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Home already exists!");
                                            max2 = 0;
                                            found = false;
                                            return true;
                                        }
                                    }
                                }
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".name", name);
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.w", w);
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.x", x);
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.y", y);
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.z", z);
                                infoConfig.save();
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved home, " + ChatColor.RED + name);
                                return true;
                            }
                        } else if (args.length == 0) {
                            if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home1.name") != null) {
                                Bukkit.broadcastMessage("m4");
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".name", "home");
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.w", w);
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.x", x);
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.y", y);
                                infoConfig.getConfig().set("Homes." + p.getUniqueId() + "home1" + ".loc.z", z);
                                infoConfig.save();
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved home, " + ChatColor.RED + "home");
                                return true;
                            }else {
                                Bukkit.broadcastMessage("e3");
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Home already exists try specifying a name!");
                                return true;
                            }
                        }
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/sethome [home-name]");
                        return true;
                    }
                }else {
                    homes = 1;
                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".ign", p.getName());
                    infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".homes", homes);
                    infoConfig.save();
                    if (args.length == 1) {
                        Bukkit.broadcastMessage("m5");
                        String name = args[0];
                        while (!found) {
                            while (max2 <= 50) {
                                max2 = max2 + 1;
                                if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home" + max2 + ".name") != null) {
                                    if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home" + max2 + ".name").equalsIgnoreCase(name)) {
                                        found = true;
                                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Home already exists!");
                                        max2 = 0;
                                        found = false;
                                        return true;
                                    }
                                }
                            }
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".name", name);
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.w", w);
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.x", x);
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.y", y);
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home" + homes + ".loc.z", z);
                            infoConfig.save();
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved home, " + ChatColor.RED + name);
                            return true;
                        }
                    } else if (args.length == 0) {
                        if (infoConfig.getConfig().getString("Homes." + p.getUniqueId() + ".home1.name") == null) {
                            Bukkit.broadcastMessage("m6");
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home1" + ".name", "home");
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home1" + ".loc.w", w);
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home1" + ".loc.x", x);
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home1" + ".loc.y", y);
                            infoConfig.getConfig().set("Homes." + p.getUniqueId() + ".home1" + ".loc.z", z);
                            infoConfig.save();
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Successfully saved home, " + ChatColor.RED + "home");
                            return true;
                        }else {
                            Bukkit.broadcastMessage("e4");
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Home already exists try specifying a name!");
                            return true;
                        }
                    }
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/sethome [home-name]");
                    return true;
                }
            }
        }else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must be a Player to execute this command!");
        }
        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("<home-name>");
            return arguments;
        }
        return null;
    }
}
