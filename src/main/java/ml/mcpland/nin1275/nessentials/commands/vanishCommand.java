package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class vanishCommand implements CommandExecutor {
    private final Nessentials plugin;
    public static Set<UUID> vanishedPlayers = new HashSet<>();
    private String prefix;

    public vanishCommand(Nessentials plugin) {
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player player) {
            if (hasVanishPermission(player)) {
                return handlePlayerCommand(player, args);
            } else {
                String noperm = plugin.getConfig().getString("No-Perm");
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
                return true;
            }
        } else {
            return handleConsoleCommand(sender, args);
        }
    }

    /**
     * Checks if the player has vanish permission
     *
     * @param player
     * @return
     */
    private boolean hasVanishPermission(Player player) {
        return player.hasPermission("nessentials.vanish") || player.hasPermission("nessentials.staff") || player.hasPermission("nessentials.admin") || player.hasPermission("nessentials.*");
    }

    /**
     * Updates the player's vanish status to all online players
     *
     * @param player
     * @param vanish
     */
    private void vanishPlayer(Player player, boolean vanish) {
        if (vanish) {
            vanishedPlayers.add(player.getUniqueId());
            for (Player otherPlayer : Bukkit.getOnlinePlayers()) {
                if (otherPlayer.equals(player)) continue;
                if (vanishedPlayers.contains(otherPlayer.getUniqueId())) {
                    otherPlayer.showPlayer(player);
                    player.showPlayer(otherPlayer);
                } else {
                    otherPlayer.hidePlayer(player);
                }
            }
        } else {
            vanishedPlayers.remove(player.getUniqueId());
            for (Player otherPlayer : Bukkit.getOnlinePlayers()) {
                if (otherPlayer.equals(player)) {
                    continue;
                }
                if (vanishedPlayers.contains(otherPlayer.getUniqueId())) {
                    player.hidePlayer(otherPlayer);
                }
                otherPlayer.showPlayer(player);
            }
        }
    }

    /**
     * Handles the command if a player executes it
     *
     * @param player
     * @param args
     * @return
     */
    private boolean handlePlayerCommand(Player player, String[] args) {
        UUID uniqueId = player.getUniqueId();
        boolean isVanished = vanishedPlayers.contains(uniqueId);
        if (args.length < 1) {
            if (isVanished) {
                vanishPlayer(player, false);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "You are now Visible.");
                player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.GRAY + "You are not in vanish anymore"));
            } else {
                vanishPlayer(player, true);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "You are now in Vanish.");
            }
            return true;
        }

        String targetName = args[0];
        Player target = Bukkit.getServer().getPlayerExact(targetName);
        UUID uniqueIdtarget = target.getUniqueId();
        boolean isVanishedTarget = vanishedPlayers.contains(uniqueIdtarget);

        if (isVanishedTarget) {
            vanishPlayer(target, false);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Set " + ChatColor.DARK_RED + target.getDisplayName() + ChatColor.GREEN + " Visible.");
            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + player.getDisplayName() + ChatColor.RED + " Made you Visible.");
        } else {
            vanishPlayer(target, true);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Set " + ChatColor.DARK_RED + target.getDisplayName() + ChatColor.GREEN + " to Vanish.");
            target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + player.getDisplayName() + ChatColor.GREEN + " Put you in Vanish.");
        }
        return true;
    }

    /**
     * Handles the command if the console executes it
     *
     * @param sender
     * @param args
     * @return
     */
    private boolean handleConsoleCommand(CommandSender sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must put a player name!");
        } else {
            String playerName = args[0];
            Player target = Bukkit.getServer().getPlayerExact(playerName);
            UUID uniqueIdtarget = target.getUniqueId();
            boolean isVanishedTarget = vanishedPlayers.contains(uniqueIdtarget);
            if (isVanishedTarget) {
                vanishPlayer(target, false);
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Set " + ChatColor.DARK_RED + target.getDisplayName() + ChatColor.GREEN + " Visible.");
                target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "CONSOLE" + ChatColor.RED + " Made you Visible.");
            } else {
                vanishPlayer(target, true);
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "Set " + ChatColor.DARK_RED + target.getDisplayName() + ChatColor.GREEN + " to Vanish.");
                target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "CONSOLE" + ChatColor.GREEN + " Put you in Vanish.");
            }
        }
        return true;
    }
}