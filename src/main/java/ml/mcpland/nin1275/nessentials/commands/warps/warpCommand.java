package ml.mcpland.nin1275.nessentials.commands.warps;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.files.infoConfig;
import ml.mcpland.nin1275.nessentials.stuff.timerUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class warpCommand implements TabExecutor {
    private final Nessentials plugin;

    public warpCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    private boolean found = false;
    private int max = 0;

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            max = 0;
            found = false;
            if (args.length == 1) {
                String name = args[0];
                while (!found) {
                    while (max <= 50) {
                        max = max + 1;
                        if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max + ".name") != null) {
                            if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max + ".name").equalsIgnoreCase(name)) {
                                found = true;
                                Location loc = new Location(Bukkit.getWorld(infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max + ".loc.w")), infoConfig.getConfig().getDouble("Warps." + p.getUniqueId() + ".warp" + max + ".loc.x"), infoConfig.getConfig().getDouble("Warps." + p.getUniqueId() + ".warp" + max + ".loc.y"), infoConfig.getConfig().getDouble("Warps." + p.getUniqueId() + ".warp" + max + ".loc.z"));
                                timerUtil.tpWarp(p, loc);
                                max = 0;
                                found = false;
                                return true;
                            }
                        }
                    }
                    max = 0;
                    found = false;
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "That warp does not exist!");
                    return true;
                }
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.WHITE + "error2");
                return true;
            }
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect usage. Do " + ChatColor.AQUA + "/warp [warp-name]");
            return true;
        }else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must be a Player to execute this command!");
        }
        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1){
            List<String> arguments = new ArrayList<>();
            Player p = (Player) sender;
            while (max <= 50) {
                max = max + 1;
                if (infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max + ".name") != null) {
                    arguments.add(infoConfig.getConfig().getString("Warps." + p.getUniqueId() + ".warp" + max + ".name"));
                }
            }
            max = 0;
            found = false;
            if (arguments.isEmpty()) {
                arguments.add("");
            }
            return arguments;
        }
        return null;
    }
}
