package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import ml.mcpland.nin1275.nessentials.files.dataConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class maintenanceCommand implements TabExecutor {

    private final Nessentials plugin;

    public maintenanceCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        String msg = this.plugin.getConfig().getString("Kick-Message");

        if (sender instanceof Player){
            Player p = ((Player) sender).getPlayer();

            if (p.hasPermission("nessentials.maintenance.admin")){

                if(args.length == 0){
                    p.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance on" + ChatColor.WHITE + " To Turn on maintenance.");
                    return true;
                }

                {
                    if (args[0].equalsIgnoreCase("on")) {
                        dataConfig.getConfig().set("maintenance", true);
                        dataConfig.save();
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "Maintenance mode on!");
                        for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                            if (!target.hasPermission("nessentials.maintenance.bypass") || !target.hasPermission("nessentials.staff") || !target.hasPermission("nessentials.admin") || !target.hasPermission("nessentials.*"))
                                target.kickPlayer(ChatColor.translateAlternateColorCodes('&', msg));
                        }
                    }
                }

                {
                    if (args[0].equalsIgnoreCase("off")) {
                        dataConfig.getConfig().set("maintenance", false);
                        dataConfig.save();
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "Maintenance mode off!");
                    }
                }

                {
                    if (args[0].equalsIgnoreCase("help")) {
                        p.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance on" + ChatColor.WHITE + " To Turn on maintenance.");
                        p.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance off" + ChatColor.WHITE + " To Turn off maintenance.");
                        p.sendMessage(ChatColor.WHITE + "Change stuff in the " + ChatColor.GOLD + "Config");
                        p.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance help" + ChatColor.WHITE + " To See this.");
                    }
                }
            }else{
                String noperm = plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }

        }else{
            if(args.length == 0){
                sender.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance on" + ChatColor.WHITE + " To Turn on maintenance.");
            }

            {
                if (args[0].equalsIgnoreCase("on")) {
                    dataConfig.getConfig().set("maintenance", true);
                    dataConfig.save();
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "Maintenance mode on!");
                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                        if (!target.hasPermission("nessentials.maintenance.bypass") || !target.hasPermission("nessentials.staff") || !target.hasPermission("nessentials.admin") || !target.hasPermission("nessentials.*"))
                            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', msg));
                    }
                }
            }

            {
                if (args[0].equalsIgnoreCase("off")) {
                    dataConfig.getConfig().set("maintenance", false);
                    dataConfig.save();
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "Maintenance mode off!");
                }
            }

            {
                if (args[0].equalsIgnoreCase("help")) {
                    sender.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance on" + ChatColor.WHITE + " To Turn on maintenance.");
                    sender.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance off" + ChatColor.WHITE + " To Turn off maintenance.");
                    sender.sendMessage(ChatColor.WHITE + "Change stuff in the " + ChatColor.GOLD + "Config");
                    sender.sendMessage(ChatColor.WHITE + "Do " + ChatColor.GOLD + "/maintenance help" + ChatColor.WHITE + " To See this.");
                }
            }
        }

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (args.length == 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("on");
            arguments.add("off");
            arguments.add("help");

            return arguments;
        }

        return null;
    }
}
