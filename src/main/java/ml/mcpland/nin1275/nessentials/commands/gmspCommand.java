package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class gmspCommand implements CommandExecutor {

    private final Nessentials plugin;

    public gmspCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player){
            Player p = ((Player) sender).getPlayer();
            if(p.hasPermission("nessentials.gmsp") || p.hasPermission("nessentials.*")){
                if (args.length == 0) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + "You are now in Gamemode Spectator!");
                    p.setGameMode(GameMode.SPECTATOR);
                } else {
                    String playerName = args[0];

                    Player target = Bukkit.getServer().getPlayerExact(playerName);

                    if (target == null) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player is not online!");
                    } else {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + target.getDisplayName() + "" + ChatColor.GREEN + " Is now in Gamemode Spectator!");
                        target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + p.getDisplayName() + "" + ChatColor.GREEN + " Set Your Gamemode to Spectator");
                        target.setGameMode(GameMode.SPECTATOR);

                    }

                }
            }else{
                String noperm = this.plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }
        }else {
            if (args.length == 1) {
                String playerName = args[0];

                Player target = Bukkit.getServer().getPlayerExact(playerName);

                if (target == null) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "That player is not online!");
                } else {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + target.getDisplayName() + "" + ChatColor.GREEN + " Is now in Gamemode Spectator!");
                    target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "CONSOLE" + "" + ChatColor.GREEN + " Set Your Gamemode to Spectator");
                    target.setGameMode(GameMode.SPECTATOR);

                }
            }else{
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Must put a player name!");
            }
        }
        return true;
    }
}
