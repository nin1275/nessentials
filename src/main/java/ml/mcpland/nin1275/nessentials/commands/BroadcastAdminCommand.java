package ml.mcpland.nin1275.nessentials.commands;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class BroadcastAdminCommand implements CommandExecutor, TabCompleter {

    private final Nessentials plugin;

    public BroadcastAdminCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player){
            Player p = (Player) sender;

            if (p.hasPermission("nessentials.adminbroadcast") || p.hasPermission("nessentials.admin") || p.hasPermission("nessentials.*")) {
                if (args.length > 0) {
                    StringBuilder brodcast = new StringBuilder();
                    for (int i = 0; i < args.length; i++) {
                        brodcast.append(args[i] + " ");
                    }

                    String bprefix = plugin.getConfig().getString("Broadcast_Prefix_Admin");
                    Bukkit.broadcast(ChatColor.translateAlternateColorCodes('&', bprefix + brodcast), "nessentials.adminbroadcast");
                    System.out.println(ChatColor.translateAlternateColorCodes('&', bprefix + brodcast));
                    for(Player player : Bukkit.getOnlinePlayers()){
                        if(player.hasPermission("nessentials.adminbroadcast") || player.hasPermission("nessentials.admin") || player.hasPermission("nessentials.*")){
                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                        }
                    }
                }else{
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage! " + ChatColor.RESET + "Do " + ChatColor.AQUA + "/adminbroadcast text");
                }
            } else {
                String noperm = plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }

        }else {
            if (args.length > 0) {
                StringBuilder brodcast = new StringBuilder();
                for (int i = 0; i < args.length; i++) {
                    brodcast.append(args[i] + " ");
                }

                String bprefix = plugin.getConfig().getString("Broadcast_Prefix_Admin");
                Bukkit.broadcast(ChatColor.translateAlternateColorCodes('&', bprefix + brodcast), "nessentials.adminbroadcast");
                System.out.println(ChatColor.translateAlternateColorCodes('&', bprefix + brodcast));
                for(Player player : Bukkit.getOnlinePlayers()){
                    if(player.hasPermission("nessentials.adminbroadcast") || player.hasPermission("nessentials.admin") || player.hasPermission("nessentials.*")){
                        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f);
                    }

                }
            }else{
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Incorrect Usage! " + ChatColor.RESET + "Do " + ChatColor.AQUA + "/adminbroadcast text");
            }
        }


        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length > 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("");
//            arguments.add(" ");

            return arguments;
        }

        if (args.length == 1){
            List<String> arguments = new ArrayList<>();
            arguments.add("");
//            arguments.add(" ");

            return arguments;
        }
        return null;
    }
}
