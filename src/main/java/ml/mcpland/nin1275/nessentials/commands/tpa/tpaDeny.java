package ml.mcpland.nin1275.nessentials.commands.tpa;

import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class tpaDeny implements CommandExecutor {
    private final Nessentials plugin;

    public tpaDeny(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String str, @NotNull String[] args) {
        String prefix = this.plugin.getConfig().getString("Prefix");
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (plugin.incomingRequest2.get(p.getUniqueId()) != null) {
                Player s = Bukkit.getPlayer(plugin.incomingRequest2.get(p.getUniqueId()));
                plugin.incomingRequest.put(s.getUniqueId(), null);
                plugin.outGoingRequest.put(s.getUniqueId(), null);
                plugin.incomingRequest2.put(p.getUniqueId(), null);
                plugin.tpaHere.put(s.getUniqueId(), null);
                s.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + p.getDisplayName() + ChatColor.RED + " Denied your tpa request!");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "Denied " + ChatColor.DARK_RED + s.getDisplayName() + ChatColor.RED + " tpa request.");
            }else {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "You don't have a tpa request.");
                return true;
            }
        }else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.DARK_RED + "Must be a Player to execute this command!");
            return true;
        }
        return true;
    }
}
