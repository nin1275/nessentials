package ml.mcpland.nin1275.nessentials.stuff;

import ml.mcpland.nin1275.nessentials.Nessentials;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jsoup.Jsoup;

import java.io.IOException;

import static ml.mcpland.nin1275.nessentials.commands.vanishCommand.vanishedPlayers;

public class timerUtil {
    private static int timerthread;
    private static int timerthread2;
    private static int timerthread3;
    private static int timerthread4;
    private static int timerthread5;
    public static void vanishActionBar() {
        final Nessentials plugin = JavaPlugin.getPlugin(Nessentials.class);
        timerthread = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(JavaPlugin.getPlugin(Nessentials.class), new Runnable() {
            @Override
            public void run() {
                for (Player players : Bukkit.getOnlinePlayers()) {
                    if (vanishedPlayers.contains(players.getUniqueId())) {
                        players.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.GRAY + "You are in vanish"));
                    }
                }
            }
        },0,20);
    }


    public static void severeMessage(Player p) {
        final Nessentials plugin = JavaPlugin.getPlugin(Nessentials.class);
        String prefix = plugin.getConfig().getString("Prefix");
        timerthread2 = plugin.getServer().getScheduler().scheduleSyncDelayedTask(JavaPlugin.getPlugin(Nessentials.class), new Runnable() {
            @Override
            public void run() {
                try {
                    String msg = Jsoup.connect("https://api.nin1275.xyz/v1/nessentials/severe.html").followRedirects(false).timeout(60000).get().body().select("#main").get(0).text();
                    if (!msg.equalsIgnoreCase("[nothing]")) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "NIN1275 " + ChatColor.GREEN + "Sent " + ChatColor.WHITE + "out " + ChatColor.AQUA + "a " + ChatColor.DARK_RED + ChatColor.BOLD + "severe " + ChatColor.RESET + ChatColor.RED + "message " + ChatColor.WHITE + "saying,\n" + ChatColor.GRAY + "---------------" + ChatColor.RED + "nessentials" + ChatColor.GRAY + "---------------\n" + ChatColor.translateAlternateColorCodes('&', msg) + "\n" + ChatColor.GRAY + "---------------" + ChatColor.RED + "nessentials" + ChatColor.GRAY + "---------------");
                    }
                } catch (IOException e) {
                    //
                }
                plugin.getServer().getScheduler().cancelTask(timerthread2);
            }
        },20);
    }
    public static void warningMessage(Player p) {
        final Nessentials plugin = JavaPlugin.getPlugin(Nessentials.class);
        String prefix = plugin.getConfig().getString("Prefix");
        timerthread3 = plugin.getServer().getScheduler().scheduleSyncDelayedTask(JavaPlugin.getPlugin(Nessentials.class), new Runnable() {
            @Override
            public void run() {
                try {
                    String msg = Jsoup.connect("https://api.nin1275.xyz/v1/nessentials/warning.html").followRedirects(false).timeout(60000).get().body().select("#main").get(0).text();
                    if (!msg.equalsIgnoreCase("[nothing]")) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.RED + "NIN1275 " + ChatColor.GREEN + "Sent " + ChatColor.WHITE + "out " + ChatColor.AQUA + "a " + ChatColor.GOLD + "warning " + ChatColor.WHITE + "saying,\n" + ChatColor.GRAY + "---------------" + ChatColor.RED + "nessentials" + ChatColor.GRAY + "---------------\n" + ChatColor.translateAlternateColorCodes('&', msg) + "\n" + ChatColor.GRAY + "---------------" + ChatColor.RED + "nessentials" + ChatColor.GRAY + "---------------");
                    }
                } catch (IOException e) {
                    //
                }
                plugin.getServer().getScheduler().cancelTask(timerthread3);
            }
        },20);
    }

    public static void tpHome(Player p, Location loc) {
        final Nessentials plugin = JavaPlugin.getPlugin(Nessentials.class);
        String prefix = plugin.getConfig().getString("Prefix");
        timerthread4 = plugin.getServer().getScheduler().scheduleSyncDelayedTask(JavaPlugin.getPlugin(Nessentials.class), new Runnable() {
            @Override
            public void run() {
                //code
                try {
                    p.teleport(loc);
                } catch (Exception e) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.WHITE + "error1-3");
                }
                plugin.getServer().getScheduler().cancelTask(timerthread4);
            }
        },plugin.getConfig().getInt("Homes.Tp-Delay")* 20L);
    }

    public static void tpWarp(Player p, Location loc) {
        final Nessentials plugin = JavaPlugin.getPlugin(Nessentials.class);
        String prefix = plugin.getConfig().getString("Prefix");
        timerthread4 = plugin.getServer().getScheduler().scheduleSyncDelayedTask(JavaPlugin.getPlugin(Nessentials.class), new Runnable() {
            @Override
            public void run() {
                //code
                try {
                    p.teleport(loc);
                } catch (Exception e) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.WHITE + "error1-3");
                }
                plugin.getServer().getScheduler().cancelTask(timerthread4);
            }
        },plugin.getConfig().getInt("Warps.Tp-Delay")* 20L);
    }

    public static void tpa(Player p, Player s) {
        final Nessentials plugin = JavaPlugin.getPlugin(Nessentials.class);
        String prefix = plugin.getConfig().getString("Prefix");
        timerthread5 = plugin.getServer().getScheduler().scheduleSyncDelayedTask(JavaPlugin.getPlugin(Nessentials.class), new Runnable() {
            @Override
            public void run() {
                //code
                try {
                    s.teleport(p.getLocation());
                } catch (Exception e) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.WHITE + "error");
                    s.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.WHITE + "error");
                }
                plugin.getServer().getScheduler().cancelTask(timerthread5);
            }
        },plugin.getConfig().getInt("Tpa.Tp-Delay")* 20L);
    }

    public static void tpaHere(Player p, Player s) {
        final Nessentials plugin = JavaPlugin.getPlugin(Nessentials.class);
        String prefix = plugin.getConfig().getString("Prefix");
        timerthread5 = plugin.getServer().getScheduler().scheduleSyncDelayedTask(JavaPlugin.getPlugin(Nessentials.class), new Runnable() {
            @Override
            public void run() {
                //code
                try {
                    p.teleport(s.getLocation());
                } catch (Exception e) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.WHITE + "error");
                    s.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.WHITE + "error");
                }
                plugin.getServer().getScheduler().cancelTask(timerthread5);
            }
        },plugin.getConfig().getInt("Tpa.Tp-Delay")* 20L);
    }
}