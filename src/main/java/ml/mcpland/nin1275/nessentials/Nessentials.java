package ml.mcpland.nin1275.nessentials;

import mc.obliviate.inventory.InventoryAPI;
import ml.mcpland.nin1275.nessentials.GUI.commands.MMCommand;
import ml.mcpland.nin1275.nessentials.commands.*;
import ml.mcpland.nin1275.nessentials.commands.homes.deleteHome;
import ml.mcpland.nin1275.nessentials.commands.homes.homeCommand;
import ml.mcpland.nin1275.nessentials.commands.homes.setHome;
import ml.mcpland.nin1275.nessentials.commands.tpa.*;
import ml.mcpland.nin1275.nessentials.commands.warps.deleteWarp;
import ml.mcpland.nin1275.nessentials.commands.warps.setWarp;
import ml.mcpland.nin1275.nessentials.commands.warps.warpCommand;
import ml.mcpland.nin1275.nessentials.files.dataConfig;
import ml.mcpland.nin1275.nessentials.files.infoConfig;
import ml.mcpland.nin1275.nessentials.listeners.*;
import ml.mcpland.nin1275.nessentials.listeners.maintenanceListener;
import ml.mcpland.nin1275.nessentials.stuff.Metrics;
import ml.mcpland.nin1275.nessentials.stuff.UpdateChecker;
import ml.mcpland.nin1275.nessentials.stuff.timerUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

public final class Nessentials extends JavaPlugin {

    private BukkitTask task;

    public HashMap<UUID, Boolean> outGoingRequest = new HashMap<>();
    public HashMap<UUID, Boolean> tpaHere = new HashMap<>();
    public HashMap<UUID, UUID> incomingRequest = new HashMap<>();
    public HashMap<UUID, UUID> incomingRequest2 = new HashMap<>();

    @Override
    public void onEnable() {
        Logger log = Bukkit.getLogger();
        // config.yml
        saveDefaultConfig();
        saveConfig();
        getConfig().options().copyDefaults();
        // data.yml
        dataConfig.load();
        // info.yml
        infoConfig.load();
       // log.info("\n" + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "NESSENTIALS" + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "Made by " + ChatColor.RED + "NIN1275" + "\n" + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "Starting Plugin" + "\n" + ChatColor.DARK_AQUA +"// " + "\n");
        new InventoryAPI(this).init();
        // Plugin startup logic

        getCommand("suicide").setExecutor(new SuicideCommand(this));
        getCommand("gmc").setExecutor(new gmcCommand(this));
        getCommand("gms").setExecutor(new gmsCommand(this));
        getCommand("gma").setExecutor(new gmaCommand(this));
        getCommand("gmsp").setExecutor(new gmspCommand(this));
        getCommand("nessentials").setExecutor(new mainCommand(this));
        getCommand("nessentials").setTabCompleter(new mainCommand(this));
        getCommand("heal").setExecutor(new healCommand(this));
        getCommand("godmode").setExecutor(new godmodeCommand(this));
        getCommand("setmotd").setExecutor(new setmotd(this));
        getCommand("setmotd").setTabCompleter(new setmotd(this));
        getCommand("broadcast").setExecutor(new BroadcastCommand(this));
        getCommand("broadcast").setTabCompleter(new BroadcastCommand(this));
        getCommand("head").setExecutor(new headCommand(this));
        getCommand("adminbroadcast").setExecutor(new BroadcastAdminCommand(this));
        getCommand("adminbroadcast").setTabCompleter(new BroadcastAdminCommand(this));
        getCommand("mainmenu").setExecutor(new MMCommand(this));
        getCommand("staffchat").setExecutor(new StaffChat(this));
        getCommand("staffchat").setTabCompleter(new StaffChat(this));
        getCommand("adminchat").setExecutor(new AdminChat(this));
        getCommand("adminchat").setTabCompleter(new AdminChat(this));
        getCommand("maintenance").setExecutor(new maintenanceCommand(this));
        getCommand("maintenance").setTabCompleter(new maintenanceCommand(this));
        getCommand("Vanish").setExecutor(new vanishCommand(this));
        getCommand("fly").setExecutor(new flyCommand(this));
        getCommand("info").setExecutor(new infoCommand(this));
        getCommand("pvp").setExecutor(new togglePVPcommand(this));
        getCommand("discord").setExecutor(new discordCommand(this));
        getCommand("sethome").setExecutor(new setHome(this));
        getCommand("sethome").setTabCompleter(new setHome(this));
        getCommand("home").setExecutor(new homeCommand(this));
        getCommand("home").setTabCompleter(new homeCommand(this));
        getCommand("deletehome").setExecutor(new deleteHome(this));
        getCommand("deletehome").setTabCompleter(new deleteHome(this));
        getCommand("setwarp").setExecutor(new setWarp(this));
        getCommand("setwarp").setTabCompleter(new setWarp(this));
        getCommand("warp").setExecutor(new warpCommand(this));
        getCommand("warp").setTabCompleter(new warpCommand(this));
        getCommand("deletewarp").setExecutor(new deleteWarp(this));
        getCommand("deletewarp").setTabCompleter(new deleteWarp(this));
        getCommand("tpa").setExecutor(new tpaCommand(this));
        getCommand("tpaaccept").setExecutor(new tpaAccept(this));
        getCommand("tpaDeny").setExecutor(new tpaDeny(this));
        getCommand("tpaCancel").setExecutor(new tpaCancel(this));
        getCommand("tpaHere").setExecutor(new tpaHere(this));

        getServer().getPluginManager().registerEvents(new joinLeaveListner(this),this);
        getServer().getPluginManager().registerEvents(new joinActionBar(this),this);
        getServer().getPluginManager().registerEvents(new HealthUnderName(this), this);
        getServer().getPluginManager().registerEvents(new maintenanceListener(this), this);
        getServer().getPluginManager().registerEvents(new vanishListener(), this);
        getServer().getPluginManager().registerEvents(new togglePvpListener(), this);
        getServer().getPluginManager().registerEvents(new messageListener(this), this);

        //task = getServer().getScheduler().runTaskTimer(this, Scoreboard.getInstance(), 0, 20);
        timerUtil.vanishActionBar();

        new UpdateChecker(this, 112117).getVersion(version -> {
            if (this.getDescription().getVersion().equals(version)) {
                getLogger().info("Using Latest Version :)");
            } else {
                getLogger().warning("There is a new update available for Nessentials. You are on " + getDescription().getVersion() + " Latest version is " + version + "!");
                getLogger().warning("Go to https://www.spigotmc.org/resources/nessentials.112117/ to download the latest version.");
            }
        });

        if (getServer().getPluginManager().getPlugin("nDuels") != null) {
            log.warning("[nessentials] Hooked to \"nDuels\" by NIN1275");
        }

        if (dataConfig.getConfig().getBoolean("maintenance")) {
            getLogger().warning("Maintenance is still on!");
        }

        // home and warps max cap and least cap saftey check
        if (getConfig().get("Homes.Max-homes") == null) {
            getConfig().set("Homes.Max-homes", 5);
            saveConfig();
        }

        if (getConfig().get("Warps.Max-warps") == null) {
            getConfig().set("Warps.Max-warps", 5);
            saveConfig();
        }

        if ((int) getConfig().get("Homes.Max-homes") > 50) {
            getConfig().set("Homes.Max-homes", 50);
            saveConfig();
        }

        if ((int) getConfig().get("Homes.Max-homes") < 0) {
            getConfig().set("Homes.Max-homes", 0);
            saveConfig();
        }

        if ((int) getConfig().get("Warps.Max-warps") > 50) {
            getConfig().set("Warps.Max-warps", 50);
            saveConfig();
        }

        if ((int) getConfig().get("Warps.Max-warps") < 0) {
            getConfig().set("Warps.Max-warps", 0);
            saveConfig();
        }

        // bStats
        int pluginId = 20449;
        Metrics metrics = new Metrics(this, pluginId);

        log.info("\n" + "\n" + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "NESSENTIALS " + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "Made by " + ChatColor.RED + "NIN1275" + "\n" + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "Loaded" + "\n" + ChatColor.DARK_AQUA +"// " + "\n");
    }

    @Override
    public void onDisable() {
        Logger log = Bukkit.getLogger();
        // Plugin shutdown logic
        if (task != null && !task.isCancelled()){
            task.cancel();
        }

        log.info("\n" + "\n" + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "NESSENTIALS " + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "Made by " + ChatColor.RED + "NIN1275" + "\n" + ChatColor.DARK_AQUA +"// " + "\n" + ChatColor.DARK_AQUA +"// " + ChatColor.AQUA + "Disabled" + "\n" + ChatColor.DARK_AQUA +"// " + "\n");
    }
}
