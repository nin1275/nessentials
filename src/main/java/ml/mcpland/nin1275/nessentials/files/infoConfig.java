package ml.mcpland.nin1275.nessentials.files;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public final class infoConfig {

    private final static infoConfig instance = new infoConfig();

    private static File file;
    private static YamlConfiguration config;

    private infoConfig() {
    }

    public static void load() {
        file = new File(Bukkit.getServer().getPluginManager().getPlugin("nessentials").getDataFolder(), "info.yml");

        if (!file.exists()) {
            Bukkit.getServer().getPluginManager().getPlugin("nessentials").saveResource("info.yml", false);
        }

        config = new YamlConfiguration();

        try {
            config.options().parseComments(true);
        } catch (Throwable t) {
            // catch
        }

        try {
            config.load(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static FileConfiguration getConfig(){
        return config;
    }

    public static void save() {
        try {
            config.save(file);
        } catch (Exception ex) {
            System.out.println("Couldn't save file");
        }
    }

    public void set(String path, Object value) {
        config.set(path, value);

        save();
    }

    public static void reload(){
        config = YamlConfiguration.loadConfiguration(file);
    }

    public static infoConfig getInstance() {
        return instance;
    }
}