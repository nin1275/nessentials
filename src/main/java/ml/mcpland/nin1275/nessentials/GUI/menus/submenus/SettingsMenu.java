package ml.mcpland.nin1275.nessentials.GUI.menus.submenus;

import mc.obliviate.inventory.Gui;
import mc.obliviate.inventory.Icon;
import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;

public class SettingsMenu extends Gui {
    private final Nessentials plugin;

    public SettingsMenu(Player player, Nessentials plugin) {
        super(player, "SettingsMenu", ChatColor.translateAlternateColorCodes('&', "&eSettings"), 3);
        this.plugin = plugin;
    }

    @Override
    public void onOpen(InventoryOpenEvent OpenEvent) {
        fillGui(Material.BLACK_STAINED_GLASS_PANE);
        addItem(12, new Icon(Material.PAPER).setName(ChatColor.translateAlternateColorCodes('&', "&b&lSet Motd")).onClick(e -> {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "Do &a/setmotd &4text"));
            player.closeInventory();
        }));
        addItem(14, new Icon(Material.JUKEBOX).setName(ChatColor.translateAlternateColorCodes('&', "&5Toggle Join Sound")).onClick(e -> {
            //player.sendMessage(ChatColor.translateAlternateColorCodes('&', "Go to the &aConfig &fand set &4\"Join-Sound\" &ffrom &6true &fto &2false &for vise versa!"));


            String prefix = this.plugin.getConfig().getString("Prefix");
            if (this.plugin.getConfig().getBoolean("Join-Sound", true)){
                plugin.getConfig().set("Join-Sound", false);
                plugin.saveConfig();
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "" + ChatColor.RED + "Turned off Join Sounds");
            }else {
                plugin.getConfig().set("Join-Sound", true);
                plugin.saveConfig();
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix) + "" + ChatColor.GREEN + "Turned on Join Sounds");
            }
            player.closeInventory();
        }));


        /*addItem(0, new Icon(Material.PLAYER_HEAD).setName(ChatColor.translateAlternateColorCodes('&', "&6&lOpen &9&lPlayerGUI")).onClick(e -> {
            player.sendMessage("sad");
        }));*/
    }
}