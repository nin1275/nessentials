package ml.mcpland.nin1275.nessentials.GUI.menus.submenus;

import mc.obliviate.inventory.Gui;
import mc.obliviate.inventory.Icon;
import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;

public class PlayerGui extends Gui {
    private final Nessentials plugin;

    public PlayerGui(Player player, Nessentials plugin) {
        super(player, "PlayerGui", ChatColor.translateAlternateColorCodes('&', "&9Player GUI"), 3);
        this.plugin = plugin;
    }

    @Override
    public void onOpen(InventoryOpenEvent OpenEvent) {
        fillGui(Material.BLACK_STAINED_GLASS_PANE);
        addItem(11, new Icon(Material.HOPPER).setName(ChatColor.translateAlternateColorCodes('&', "&4&lClear Inventory")).onClick(e -> {
            player.closeInventory();
            player.getInventory().clear();
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aInventory Cleared"));
        }));
        addItem(13, new Icon(Material.GOLDEN_APPLE).setName(ChatColor.translateAlternateColorCodes('&', "&6Refill your Health")).onClick(e -> {
            player.closeInventory();
            player.performCommand("heal");
        }));
        addItem(15, new Icon(Material.TNT).setName(ChatColor.translateAlternateColorCodes('&', "&4Suicide")).onClick(e -> {
            player.closeInventory();
            player.performCommand("suicide");
        }));


        /*addItem(0, new Icon(Material.PLAYER_HEAD).setName(ChatColor.translateAlternateColorCodes('&', "&6&lOpen &9&lPlayerGUI")).onClick(e -> {
            player.sendMessage("sad");
        }));*/
    }
}