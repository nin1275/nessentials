package ml.mcpland.nin1275.nessentials.GUI.menus;

import mc.obliviate.inventory.Gui;
import mc.obliviate.inventory.Icon;
import ml.mcpland.nin1275.nessentials.GUI.menus.submenus.PlayerGui;
import ml.mcpland.nin1275.nessentials.GUI.menus.submenus.SettingsMenu;
import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;

public class MMGUI extends Gui {
    private final Nessentials plugin;
    public MMGUI(Player player, Nessentials plugin) {
        super(player, "mainmenu", ChatColor.translateAlternateColorCodes('&', "&6&lMain Menu"), 3);
        this.plugin = plugin;
    }

    @Override
    public void onOpen(InventoryOpenEvent OpenEvent) {
        fillGui(Material.BLACK_STAINED_GLASS_PANE);
        addItem(14, new Icon(Material.PLAYER_HEAD).setName(ChatColor.translateAlternateColorCodes('&', "&6&lOpen &9&lPlayerGUI")).onClick(e -> {
            player.closeInventory();
            new PlayerGui(player, plugin).open();
        }));
        addItem(12, new Icon(Material.BUCKET).setName(ChatColor.translateAlternateColorCodes('&', "&6&lOpen &e&lSettings")).onClick(e -> {
            player.closeInventory();
            new SettingsMenu(player, plugin).open();
        }));


        /*addItem(0, new Icon(Material.PLAYER_HEAD).setName(ChatColor.translateAlternateColorCodes('&', "&6&lOpen &9&lPlayerGUI")).onClick(e -> {
            player.sendMessage("sad");
        }));*/
    }
}