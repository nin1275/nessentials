package ml.mcpland.nin1275.nessentials.GUI.commands;

import ml.mcpland.nin1275.nessentials.GUI.menus.MMGUI;
import ml.mcpland.nin1275.nessentials.Nessentials;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class MMCommand implements CommandExecutor {
    private final Nessentials plugin;

    public MMCommand(Nessentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        Player p = (Player) sender;
        if (sender instanceof Player) {
            if (!p.hasPermission("nessentials.gui.menugui") || !p.hasPermission("nessentials.*") || !p.hasPermission("nessentials.admin")) {
                String noperm = plugin.getConfig().getString("No-Perm");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', noperm));
            }else {
                new MMGUI(p, plugin).open();
            }
        }else {
            sender.sendMessage(ChatColor.RED + "Must be a Player to execute this command!");
        }

        return true;
    }
}
